<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
    <title>Akvarel</title>
    <style>
      * {margin:0;padding:0}
      body,html{height:100%}
      body{font-family:'Lato';font-weight:300;font-size:16px;line-height:24px;color:#000}
      .table{display:table;width:100%;height:100%;}
      .cell{display:table-cell;width:100%;height:100%;vertical-align:middle;}
      table{margin:0 auto;}
      table td{width:33.33%;vertical-align:top;text-align:left;padding:0 50px;}
      table td div{margin: 4px 0;}
      a{color:#000;text-decoration:none;border-bottom:1px solid rgba(0,0,0,0.2)}
      a:hover{color:red;border-color:#f00}
      span{display:inline-block;font-size:13px;color:green;width:24px;text-align:right;margin-right:5px;}
    </style>
</head>

<body>

  <?php
    error_reporting(E_ALL);

    $files = array();
    foreach (glob('./*.html') as $file) {
      $filename = basename($file);
      array_push($files, $filename);
    }

    $columns = 2;
    $count = ceil(count($files) / $columns);
    $chunk = array_chunk($files, $count);
  ?>

  <div class="table">
    <div class="cell">
      <table>
        <tr>
          <?php for ($i = 0; $i < ($columns); $i++): ?>
            <td>
              <?php foreach ($chunk[$i] as $j => $item): ?>
                <?php $index = ($i * $count + $j + 1); ?>
                <div>
                  <span><?=$index?>.</span>
                  <a href="<?=$item?>"><?=$item?></a>
                </div>
              <?php endforeach; ?>
            </td>
          <?php endfor; ?>
        </tr>
      </table>
    </div>
  </div>

</body>

</html>