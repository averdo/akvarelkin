$(document).ready(function(){
	$('.slick.carousel').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					dots: false,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1023,
				settings: {
					dots: false,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
		],
		prevArrow: false,
		nextArrow: false
	});
	
	$('.slick.brands').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1023,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			}
		],
		prevArrow: '<i class="arrow_prev arrow"></i>',
		nextArrow: '<i class="arrow_next arrow"></i>'
	});
	
	$('.slick.new').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1023,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1199,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			}
		],
		prevArrow: '<i class="arrow_prev arrow border"></i>',
		nextArrow: '<i class="arrow_next arrow border"></i>'
	});
	
	$('.slick.offer_series_m').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		],
		prevArrow: false,
		nextArrow: '<i class="change_slide next_item">Следующие товары</i>'
	});
	
	$('.slick.view').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 5,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1023,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1254,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				}
			}
		],
		prevArrow: '<i class="arrow_prev arrow"></i>',
		nextArrow: '<i class="arrow_next arrow"></i>'
	});
	
	$('.element .slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider-nav'
	});
	
	$('.element .slider-nav').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.slider-for',
		dots: false,
		arrows: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 1023,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1254,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				}
			}
		]
	});

    $('.form1 input[type="radio"]').checkboxradio();
	$('.form1 select').selectmenu();
	$('.form1 input[type="checkbox"]').checkboxradio();
	$('.form2 input[type="checkbox"]').checkboxradio();
	$('.form2 select.oneselect').selectmenu({});
    $('.datepicker input').datepicker({
		autoSize: true,
		firstDay: 0,
		showWeek: false,
		prevText: "",
		nextText: "",
		dayNamesMin: [ "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс" ],
		monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
		beforeShow: function () {
			
		}
	});
	
	Inputmask().mask(document.querySelectorAll("input"));

	function initCatalogMenu()
	{
		if (window.innerWidth <= 1023)
		{
			$('.js-catalog-menu').on('click', function(){
				$('.catalog_menu').toggleClass('is_open');
				$('.box_shadow').toggleClass('active');
				return false;
			});
		}
	}
	
	$(window).resize(function() {
		initCatalogMenu();
	})
	
	initCatalogMenu();
	
	$('.js-second-menu').on('click', function(){
		$('.second_menu').toggleClass('is_open');
		$('.box_shadow').toggleClass('active');
		return false;
	});
	
	$('.catalog_menu .level-0 > li > a').on('click', function(){
		$('.catalog_menu .level-0 li').not($(this).parent()).removeClass('active');
		$(this).parent().toggleClass('active');
		return false;
	})
	
	$('.second_menu .level-0 > li > a').on('click', function(){
		$('.second_menu .level-0 li').not($(this).parent()).removeClass('active');
		$(this).parent().toggleClass('active');
		return false;
	})
	
	$('.b_select_filter_by_param .js-del').on('click', function(){
		$(this).parents('.prop').remove();
		if ($('.b_select_filter_by_param .prop').length <= 0)
		{
			$('.b_select_filter_by_param').remove();
		}
		return false;
	})
	
	$('.box_shadow').on('click', function(){
		$(this).removeClass('active');
		$('.is_open').removeClass('is_open');
		$('body').removeClass('noscroll');
		return false;
	})
	
	$('.top_menu .level-0 li > a').on('click', function(){
		$('.top_menu .level-0 li > a').not($(this)).removeClass('active');
		$('.top_menu .level-1').not($(this).next()).removeClass('active');
		$(this).toggleClass('active');
		$(this).next().toggleClass('active');
		return false;
	})
	
	$(document).mouseup(function (e){ // событие клика по веб-документу
		var div = $(".top_menu"); // тут указываем элемент
		if (!div.is(e.target) // если клик был не по нашему блоку
		    && div.has(e.target).length === 0) { // и не по его дочерним элементам
			$('.top_menu .level-0 li > a').removeClass('active');
			$('.top_menu .level-1').removeClass('active');
		}
	});
	
	$('.element .js-tabs').on('click', function(){
		$('.element .js-tabs').removeClass('active');
		$(this).addClass('active');
		$('.offer_tabs_data .b_tab').removeClass('active');
		$('.' + $(this).data('for')).addClass('active');
		return false;
	})
	
	$('.js-popup').on('click', function(){
		var popup_id = $(this).data('popup');
		$('.is_open').removeClass('is_open');
		$('#' + popup_id).addClass('is_open');
		$('.box_shadow').addClass('active');
		$('body').addClass('noscroll');
		$('#' + popup_id + ' .body').css({height: $(window).height() - 50 + 'px'});
		return false;
	});
	
	if (screen.width < 992)
	{
		$( window ).resize(function(){
			$('.popup .body').css({height: $(window).height() - 50 + 'px'});
		});
	}
	
	$('.js-close-popup').on('click', function(){
		$('.is_open').removeClass('is_open');
		$('.box_shadow').removeClass('active');
		$('body').removeClass('noscroll');
		return false;
	});
	
	$('.form2 .custom_select .custom_field').on('click', function(){
		$(this).parent().toggleClass('active');
		return false;
	});
	
	$(document).mouseup(function (e){ // событие клика по веб-документу
		var div = $(".form2 .custom_select.active"); // тут указываем элемент
		if (!div.is(e.target) // если клик был не по нашему блоку
		    && div.has(e.target).length === 0){ // и не по его дочерним элементам
			$('.form2 .custom_select.active').removeClass('active');
		}
	});
	
	$('.js-add-to-cart').on('click', function(){
		$(this).closest('.in_cart').toggleClass('active');
		return false;
	})
	
	$('.js-add-comment').on('click', function(){
		$('.post_list .post.add_post').show();
		return false;
	});


	/*STICKY BUTTON SHOW*/

	if (window.matchMedia('(min-width: 992px)').matches) {
		var fieldWidthSticky = $('.step1').width();
		var fieldOffsetLeftSticky = $('.step1').offset().left;
		$('.scrolling-btn').width(fieldWidthSticky);
		$('.scrolling-btn').offset({ 'left': fieldOffsetLeftSticky });

		window.onscroll = function () {
			var checkTop = $('.checkTop').offset().top;
			var wTop = $(window).scrollTop() + $(window).height();
			if (checkTop > wTop) {
				$('.scrolling-btn').css({
					'position': 'fixed'
				})
			}
			else {
				$('.scrolling-btn').css({
					'position': 'static'
				})
			}
		};
		
	}

	if (window.matchMedia('(min-width: 320px) and (max-width: 767px)').matches) {
		function chainBtn () {
			if ($('#catalog_filter').hasClass('is_open')) {
				$('.scrolling-btn').addClass('scrolling-btn-mobile');
			}
		}		
		$('#btn_filter_by_param').click (function (){
			setTimeout(chainBtn, 1000);
		});
		$('.js-close-popup').click(function () {
			$('.scrolling-btn').removeClass('scrolling-btn-mobile');
		});


		$('.body').scroll(function () {
			if ($(this).scrollTop() >= this.scrollHeight - $(this).height() - 200) {
				$('.scrolling-btn-mobile').css({
					'opacity': '0'
				})
			}
			else {
				$('.scrolling-btn-mobile').css({
					'opacity': '1'
				})
			}

			if ($(this).scrollTop() >= this.scrollHeight - $(this).height() - 100) {
				$('.scrolling-btn-mobile').css({
					'position': 'static',
					'transform': 'translateX(0)'
				})
			}
			else {
				$('.scrolling-btn-mobile').css({
					'position': 'fixed',
					'transform': 'translateX(-50%)'
				})
			}

			if ($(this).scrollTop() >= this.scrollHeight - $(this).height()) {
				$('.scrolling-btn-mobile').css({
					'opacity': '1'
				})
			}
		});

		
	}
	
});